PLAYING CARDS GAME

This game is PHP and JQuery based single player memory match card game,In the
memory match card game, all cards are laid face down on a table or other flat surface.
and players take turns flipping pairs of cards over. On each turn, the player will first turn
one card over, then a second. If the two cards match, the player scores two points, the two
cards are removed from the game, and the player gets another turn. If they do not match,
the cards are turned back over.
The object of the game is to collect the most matching pairs. At the start of the game the
12 cards are ‘flipped over’ and randomly placed on the board.
This game display grid with 12 deck. Each deck has 6 pairs of randomly card which
makes it total of 12 cards.
Features of Game
Time clock for each game round (2 MINUTES).
Current score of the player and ability to save score with player name.
Leader Board.

Tools used for development:

Laravel framework 5.1
PHP 5.5
Jquery
Mysql
Apache