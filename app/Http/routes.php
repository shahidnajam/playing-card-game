<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', array(
    'as' => 'main',
    'uses' => 'HomeController@main'
));

Route::get('start', array(
    'as' => 'start',
    'uses' => 'HomeController@getStart'
));

Route::post('start', array(
    'as' => 'start.post',
    'uses' => 'HomeController@postStart'
));

Route::get('end', array(
    'as' => 'end',
    'uses' => 'HomeController@end'
));


/**
 * App ajax call related routes
 */
Route::get('game/flipcard', array(
    'as' => 'game.flipcard',
    'uses' => 'GameController@flipCard'
));

