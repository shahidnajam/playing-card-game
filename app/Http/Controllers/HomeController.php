<?php

namespace App\Http\Controllers;

use Redirect, Session, View, Input, Validator;
use App\Models\User;
use App\CG\Contracts\Services\Deck as DeckContract;
use App\CG\Contracts\Repositories\Leaderboard;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * @var DeckContract
     */
    protected $_deck;

    /**
     * @var Leaderboard
     */
    protected $_leaderboard;

    /**
     * @param DeckContract $deck
     * @param Leaderboard $leaderboard
     */
    public function __construct(DeckContract $deck, Leaderboard $leaderboard)
    {
        $this->_deck = $deck;
        $this->_leaderboard = $leaderboard;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function main()
    {
        $user = Session::get('user.user');

        if (!$user) {
            return Redirect::to('start');
        }

        // Reset session and draw new deck from beginning
        if (Session::has('user.deck')) {
            $deck = Session::get('user.deck');
            $deck->drawNew();

            Session::set('user.deck', $deck);
        }

        $viewData = array(
            'name' => $user->name,
            'totalScore' => $this->_leaderboard->getScore($user->id),
            'topUsers' => $this->_leaderboard->getTop(5)
        );

        return View::make('frontend.game')->with($viewData);
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function getstart()
    {
        return View::make('frontend.start');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postStart()
    {
        Session::flush();

        $validator = Validator::make(
            Input::only('name'),
            array('name' => 'required|min:3')
        );

        if ($validator->fails()) {
            return Redirect::to('start')->with('message', 'Name is too short.');
        }

        try {
            // Initialize session for user with new game
            $user = User::create(array('name' => Input::get('name')));

            $this->_leaderboard->incrementScore($user->id, 0);
            $this->_deck->drawNew();

            Session::set('user.user', $user);
            Session::set('user.deck', $this->_deck);
        } catch (Exception $e) {
            return Redirect::to('start')->with('message', 'Error: unable to start game');
        }

        return Redirect::to('/');
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function end()
    {
        if (Session::has('user.deck')) {
            $deck = Session::get('user.deck');
            $deck->drawNew();
            Session::set('user.deck', $deck);
        }

        return View::make('frontend.end');
    }
}