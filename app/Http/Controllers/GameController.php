<?php

namespace App\Http\Controllers;

use Session, Response, Input, Exception, Config, Validator;
use App\CG\Contracts\Repositories\Leaderboard;

/**
 * Class GameController
 * @package App\Http\Controllers
 */
class GameController extends Controller
{
    /**
     * @var Leaderboard
     */
    protected $_leaderboard;

    /**
     * GameController constructor.
     * @param Leaderboard $leaderboard
     */
    public function __construct(Leaderboard $leaderboard)
    {
        $this->_leaderboard = $leaderboard;

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function flipCard()
    {
        $deck = Session::get('user.deck');
        $user = Session::get('user.user');

        $params = Input::only(array('row', 'col'));

        //Checking if row and col params from request are inside the range board rows and cols
        $rows = Config::get('cg.deck.size.rows');
        $cols =  Config::get('cg.deck.size.cols');

        $rules = array(
            'row' => 'required|digits_between:0,'.($rows - 1),
            'col' => 'required|digits_between:0,'.($cols - 1)
        );

        $validator = Validator::make($params,$rules);

        if ($validator->fails() || is_null($deck) || is_null($user)) {
            $response['error'] = true;
            return Response::json($response, 422);
        }

        try {
            $card = $deck->getCard($params['row'], $params['col']);

            if (!$card) {
                throw new Exception('No card on specified position or already flipped');
            }

            $lastMove = $deck->getFirstTry();
            $cardMatched = $deck->flipCard($params['row'], $params['col']);
            $cardPicture = asset(Config::get('cg.card.picture.paths.folder').DIRECTORY_SEPARATOR.$card->getPicture().'.'.Config::get('cg.card.picture.extention'));

            $data = array(
                'game_over' => $deck->isGameOver(),
                'score' => $deck->getScore(),
                'card_picture' => $cardPicture
            );

            /*
            * According to game rules Check if card we will save new score
            * Otherwise if both doesn't match, we should flip the card back
            */
            if ($cardMatched) {
                $data ['matched'] = 1;
                $data['user_total_score'] = $this->_leaderboard->incrementScore($user->id, Config::get('cg.match_score'));
            } else if ($lastMove) {
                $data ['matched'] = 0;
                $data['flipped_picture'] = asset(Config::get('cg.card.picture.paths.flipped'));
            }
            return Response::json(['error' => false, 'result' => $data], 200);
        } catch (Exception $e) {
            throw $e;
            return Response::json(['error' => true, 'result' => null], 420);
        }
    }
}