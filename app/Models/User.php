<?php namespace App\Models;

class User extends \Eloquent{

    // Protected fields for mass assignment
    protected $guarded = array('id');
}
