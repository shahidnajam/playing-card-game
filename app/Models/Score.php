<?php namespace App\Models;

class Score extends \Eloquent{
	// Protected fields for mass assignment
	protected $guarded = array('id');
	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}

}
