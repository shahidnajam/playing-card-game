<?php

namespace App\CG\Repositories\Leaderboard;

use App\Models\Score;
use App\CG\Services\Cache\Cacher;
use Illuminate\Support\ServiceProvider;

class LeaderboardServiceProvider extends ServiceProvider
{
    /**
     * Register the binding
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\CG\Contracts\Repositories\Leaderboard', function($app)
        {
            return new LeaderboardRepository(
                new Score,
                new Cacher($app['cache'], 'leaderboard', 10)
            );
        });
    }
}