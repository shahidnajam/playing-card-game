<?php

namespace App\CG\Repositories\Leaderboard;

use App\CG\Contracts\Repositories\Leaderboard as LeaderboardContract;
use App\CG\Contracts\Services\Cacher;
use App\Models\Score;

/**
 * Class CurrencyRepository
 * @package App\CC\Repositories\Currency
 */
class LeaderboardRepository implements LeaderboardContract
{
    /**
     * @var Score
     */
    protected $_modelScore;

    /**
     * @var Cacher
     */
    protected $_cache;

    /**
     * @param Score $modelScore
     * @param Cacher $cache
     */
    public function __construct(Score $modelScore, Cacher $cache)
    {
        $this->_modelScore = $modelScore;
        $this->_cache = $cache;
    }

    /**
     * Get user list with top scores
     *
     * @param $limit
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getTop($limit)
    {
        // Build the unique cache key
        $key = md5('top.limit.'.$limit);

        if ($this->_cache->has($key)) {
            return $this->_cache->get($key);
        }

        $userScores = $this->_modelScore->with('user')->where('score', '>', 0)
            ->orderBy('score', 'desc')
            ->limit($limit)
            ->get();

        // Store in cache for next request
        $this->_cache->put($key, $userScores);

        return $userScores;
    }

    /**
     * Get user score by user id
     *
     * @param $id
     * @return int
     */
    public function getScore($id)
    {
        $modelScore = $this->_modelScore->where('user_id', $id)->firstOrFail();

        return $modelScore->score;
    }

    /**
     * Increment score for user
     *
     * @param $id
     * @param $score
     * @return int
     */
    public function incrementScore($id, $increment)
    {
        $modelScore = $this->_modelScore->firstOrNew(array('user_id' => $id));

        $modelScore->increment('score', $increment);
        $modelScore->save();

        return $modelScore->score;
    }
}