<?php

namespace App\CG\Contracts\Services\Card;

/**
 * Interface Type
 * @package App\CG\Contracts\Services\Card
 */
interface Type{

    /**
     * Get name for card type
     *
     * @return mixed
     */
    public function getName();
}