<?php

namespace App\CG\Contracts\Services;

/**
 * Interface Deck
 * @package App\CG\Contracts\Services
 */
interface Deck
{
    /**
     * Draw deck with pair of card faces with different types
     *
     * @return mixed
     */
    public function drawNew();

    /**
     * @return int
     */
    public function getScore();

    /**
     * Flip card and return the result whether try was successful or failed
     *
     * @param $row
     * @param $col
     * @return bool
     */
    public function flipCard($row, $col);

    /**
     * Get position(row,col) for first try in case of second try of flipping
     *
     * @return array
     */
    public function getFirstTry();

    /**
     * Check if game is over
     *
     * @return bool
     */
    public function isGameOver();

    /**
     * Get card object from deck by row and column
     *
     * @return App\CG\Contracts\Services\Card
     */
    public function getCard($row, $col);
}