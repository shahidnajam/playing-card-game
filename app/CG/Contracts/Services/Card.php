<?php

namespace App\CG\Contracts\Services;

/**
 * Interface Card
 * @package App\CG\Contracts\Services
 */
interface Card{

    /**
     * @return App\CG\Contracts\Services\Card\Type
     */
    public function getType();

    /**
     * Get name for card
     *
     * @return string
     */
    public function getName();

    /**
     * Get picture filename for card
     *
     * @return string
     */
    public function getPicture();
}