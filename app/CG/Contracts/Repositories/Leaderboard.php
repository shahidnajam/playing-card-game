<?php

namespace App\CG\Contracts\Repositories;

/**
 * Interface Leaderboard
 * @package App\CG\Contracts\Repositories
 */
interface Leaderboard
{
    /**
     * Get user list with top scores
     *
     * @param $limit
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getTop($limits);

    /**
     * Get user score by user id
     *
     * @param $id
     * @return int
     */
    public function getScore($id);

    /**
     * Increment score for user
     *
     * @param $id
     * @param $score
     * @return int
     */
    public function incrementScore($id, $increment);
}