<?php

namespace App\CG\Services\Card;

/**
 * Class Joker
 * @package App\CG\Services\Card
 */
class Joker extends AbstractCard
{
    /**
     * Get name for Joker card
     *
     * @return string
     */
    public function getName()
    {
        return 'Joker';
    }

} 