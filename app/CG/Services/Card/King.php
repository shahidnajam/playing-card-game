<?php

namespace App\CG\Services\Card;

/**
 * Class King
 * @package App\CG\Services\Card
 */
class King extends AbstractCard
{
    /**
     * Get name for King card
     *
     * @return string
     */
    public function getName()
    {
        return 'King';
    }

} 