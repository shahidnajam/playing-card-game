<?php

namespace App\CG\Services\Card;

/**
 * Class Ace
 * @package App\CG\Services\Card
 */
class Ace extends AbstractCard
{
    /**
     * Get name for Ace card
     *
     * @return string
     */
    public function getName()
    {
        return 'Ace';
    }

} 