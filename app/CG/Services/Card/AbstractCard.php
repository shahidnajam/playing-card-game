<?php

namespace App\CG\Services\Card;

use App\CG\Contracts\Services\Card as CardContract;
use App\CG\Contracts\Services\Card\Type as CardTypeContract;

/**
 * Class AbstractCard
 * @package App\CG\Services\Card
 */
abstract class AbstractCard implements CardContract
{

    /**
     * @var CardTypeContract
     */
    protected $_type;

    /**
     * AbstractCard constructor.
     * @param CardTypeContract $cardType
     */
    public function __construct(CardTypeContract $cardType)
    {
        $this->_type = $cardType;
    }

    /**
     * Get card type
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->_type;

    }

    /**
     * Get picture for card
     *
     * @return string
     */
    public function getPicture()
    {
        //Return card image with first letter of card and its type
        //i.e Card King Diamond will return 'K_diamond' (first letter with underscore and type)
        return ucfirst(substr($this->getName(), 0, 1)).'_'.strtolower($this->getType()->getName());
    }
}