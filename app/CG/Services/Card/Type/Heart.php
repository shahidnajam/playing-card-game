<?php

namespace App\CG\Services\Card\Type;

use App\CG\Contracts\Services\Card\Type as CardTypeContract;

/**
 * Class Heart
 * @package App\CG\Services\Card\Type
 */
class Heart implements CardTypeContract
{
    /**
     * Get name for Heart
     *
     * @return string
     */
    public function getName()
    {
        return 'Heart';
    }

} 