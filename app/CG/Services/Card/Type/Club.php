<?php

namespace App\CG\Services\Card\Type;

use App\CG\Contracts\Services\Card\Type as CardTypeContract;

/**
 * Class Club
 * @package App\CG\Services\Card\Type
 */
class Club implements CardTypeContract
{
    /**
     * Get name for Club
     *
     * @return string
     */
    public function getName()
    {
        return 'Club';
    }
}