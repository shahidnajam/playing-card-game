<?php

namespace App\CG\Services\Card\Type;

use App\CG\Contracts\Services\Card\Type as CardTypeContract;

/**
 * Class Queen
 */
class Diamond implements CardTypeContract
{
    /**
     * Get name for Diamond
     *
     * @var string
     */
    public function getName()
    {
        return 'Diamond';
    }
}