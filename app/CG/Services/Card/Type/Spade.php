<?php

namespace App\CG\Services\Card\Type;

use App\CG\Contracts\Services\Card\Type as CardTypeContract;

/**
 * Class Spade
 * @package App\CG\Services\Card\Type
 */
class Spade implements CardTypeContract
{
    /**
     * Get name for Spade
     *
     * @return string
     */
    public function getName()
    {
        return 'Spade';
    }
}