<?php

namespace App\CG\Services\Card;

use Exception;

/**
 * Class CardFactory
 * @package App\CG\Services\Card
 */
class CardFactory
{
    /**
     * Get card with face type
     *
     * @param $card
     * @param $type
     * @return mixed
     * @throws Exception
     */
    public static function build($card, $type)
    {
        $card = 'App\\CG\\Services\\Card\\'.ucfirst($card);
        $type = 'App\\CG\\Services\\Card\\Type\\'.ucfirst($type);

        // Check if both card and face type are valid classes
        if (class_exists($card) && class_exists($type)) {
            return new $card(new $type);
        } else {
            throw new Exception("Invalid Card face given.");
        }
    }
}