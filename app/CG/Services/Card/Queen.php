<?php

namespace App\CG\Services\Card;

/**
 * Class Queen
 * @package App\CG\Services\Card
 */
class Queen extends AbstractCard
{
    /**
     *
     * Get name for Queen card
     *
     * @return string
     */
    public function getName()
    {
        return 'Queen';
    }
}