<?php

namespace App\CG\Services\Cache;

use Illuminate\Cache\CacheManager as Cache;
use App\CG\Contracts\Services\Cacher as CacheContract;

/**
 * Class Cacher
 * @package App\CG\Services\Cache
 */
class Cacher implements CacheContract
{
    /**
     * @var Cache
     */
    protected $cache;
    /**
     * @var string
     */
    protected $cachekey;
    /**
     * @var int
     */
    protected $minutes;

    /**
     * @param Cache $cache
     * @param $cachekey
     * @param null $minutes
     */
    public function __construct(Cache $cache, $cachekey, $minutes = null)
    {
        $this->cache = $cache;
        $this->cachekey = $cachekey;
        $this->minutes = $minutes;
    }

    /**
     * Retrieve data from cache
     *
     * @param string    Cache item key
     * @return mixed    PHP data result of cache
     */
    public function get($key)
    {
        return $this->cache->tags($this->cachekey)->get($key);
    }

    /**
     * Add data to the cache
     *
     * @param string    Cache item key
     * @param mixed     The data to store
     * @param integer   The number of minutes to store the item
     * @return mixed    $value variable returned for convenience
     */
    public function put($key, $value, $minutes = null)
    {
        if (is_null($minutes)) {
            $minutes = $this->minutes;
        }

        return $this->cache->tags($this->cachekey)->put($key, $value, $minutes);
    }

    /**
     * Test if item exists in cache
     * Only returns true if exists && is not expired
     *
     * @param string    Cache item key
     * @return bool     If cache item exists
     */
    public function has($key)
    {
        return $this->cache->tags($this->cachekey)->has($key);
    }
}