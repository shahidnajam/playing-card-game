<?php

namespace App\CG\Services\Deck;

use App, Config;
use App\CG\Contracts\Services\Deck as DeckContract;

/**
 * Class Deck
 * @package App\CG\Services\Deck
 */
class Deck implements DeckContract
{
    /**
     * @var array
     */
    protected $_cards = array(
        'Ace',
        'Joker',
        'King',
        'Queen'
    );
    /**
     * @var array
     */
    protected $_cardTypes = array(
        'Club',
        'Diamond',
        'Heart',
        'Spade'
    );

    /**
     * @var array
     */
    protected $_deck;

    /**
     * @var array
     */
    protected $_score = 0;

    /**
     * @var array
     */
    protected $_lastMovePosition = array();

    /**
     * Draw deck with pair of card faces with different types
     */
    public function drawNew()
    {
        $this->_deck = array();
        $this->_score = 0;
        $this->_lastMovePosition = null;

        $rows = Config::get('cg.deck.size.rows');
        $cols =  Config::get('cg.deck.size.cols');

        $totalBoardDeck = $rows * $cols;

        // We divide by 2 to make pairs of each card
        // i.e In case of 12 total of (rows * cols) , We need to put 6 card with different type
        $totalFaces = $totalBoardDeck / 2;

        $cards = array();
        $cardPairs = array();

        // Get all possible combination of card face with type
        foreach ($this->_cards as $card) {
            foreach ($this->_cardTypes as $cardType) {
                $cards[] = App\CG\Services\Card\CardFactory::build($card, $cardType);
            }
        }

        // We need pick up some of randoms (depending on config) from all total combination
        $randKeys = array_rand($cards, $totalFaces);
        foreach ($randKeys as $key => $value) {
            array_push($cardPairs, $cards[$value]);
        }

        // Make pair of each randomly taken card and shuffle the order
        $cardPairs = array_merge($cardPairs, $cardPairs);
        shuffle($cardPairs);

        // Draw and put random picked cards on deck
        for ($x = 0; $x < $rows; $x++) {
            for ($y = 0; $y < $cols; $y++) {
                $this->_deck[$x][$y] = array_shift($cardPairs);
            }
        }
    }

    /**
     * @return array
     */
    public function getScore()
    {
        return $this->_score;
    }

    /**
     * Flip card and return the result whether try was successful or failed
     *
     * @param $row
     * @param $col
     * @return bool
     */
    public function flipCard($row, $col)
    {
        // Check if it is first or second try
        if (!is_null($this->_lastMovePosition)) {
            $lastMoveCard = $this->_deck[$this->_lastMovePosition[0]][$this->_lastMovePosition[1]];
            // According to game rules if current card/type match with last card/type, it should be removed
            if ($this->_deck[$row][$col] === $lastMoveCard) {
                //Second try is successful , Remove last two cards from deck
                $this->_deck[$this->_lastMovePosition[0]][$this->_lastMovePosition[1]] = null;
                $this->_deck[$row][$col] = null;
                $this->_lastMovePosition = null;
                $this->_score += Config::get('cg.match_score');
                return true;
            }
            // Second try fail so we set last position empty
            $this->_lastMovePosition = null;
            return false;
        }
        // In case of every first try, Set current move for next move to match
        $this->_lastMovePosition = array($row, $col);

        return false;
    }

    /**
     * Get position(row,col) for first try in case of second try of flipping
     *
     * @return array
     */
    public function getFirstTry()
    {
        return $this->_lastMovePosition;
    }

    /**
     * Check if game is over (If any card left on deck)
     *
     * @return bool
     */
    public function isGameOver()
    {
        // We filter recursively to remove nulls and check if any card left on the deck
        // If no card left , it means game is over
        $cardLeft = array_filter(array_map('array_filter', $this->_deck));
        return count($cardLeft) == 0;
    }

    /**
     * Get card object from deck by row and column
     *
     * @param int $row
     * @param int $col
     * @return App\CG\Contracts\Services\Card
     */
    public function getCard($row, $col)
    {
        return $this->_deck[$row][$col];
    }
}