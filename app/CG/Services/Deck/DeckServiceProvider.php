<?php

namespace App\CG\Services\Deck;

use Illuminate\Support\ServiceProvider;

/**
 * Class DeckServiceProvider
 * @package App\CG\Services\Deck
 */
class DeckServiceProvider extends ServiceProvider
{
    /**
     * Register the binding
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\CG\Contracts\Services\Deck', function($app)
        {
            return new Deck;
        });
    }
}