<?php

return [

    // Score to increment when two cards are matched
    'match_score' => 2,

    'deck' => [
        'size' => [
            'rows' => 3,
            'cols' => 4,
        ]
    ],

    'card' => [
        'picture' => [
            'extention' => 'png',
            'paths' => [
                'folder' => 'assets/img',
                'flipped' => 'assets/img/flipped_card.png',
            ]
        ]
    ]
];
