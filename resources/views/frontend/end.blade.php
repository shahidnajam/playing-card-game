@extends('frontend.layouts.base')

@section('content')
    <div class="col-lg-9 col-offset-6 centered">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Thank you for playing the game</h3>
            </div>
            <div class="panel-body">
                Would you like to play again?
            </div>
            <p class="text-center">
                <a class="btn btn-primary" href="{{action('HomeController@main')}}" role="button">Yes</a>
                <a class="btn btn-default" href="{{action('HomeController@getStart')}}" role="button">No</a>
            </p>
        </div>
    </div>
@stop