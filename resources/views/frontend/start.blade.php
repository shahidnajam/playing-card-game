@extends('frontend.layouts.base')

@section('content')
    <div class="row" style="margin-top:100px;">
        <div class="col-md-4 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><strong>Playing Card Game</strong></h3></div>
                <div class="panel-body">
                    <form action="{{action('HomeController@postStart') }}" method="post">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input class="form-control" name="name" type="text" value="">
                        </div>
                        <button type="submit" class="btn btn-sm btn-default">Start</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop