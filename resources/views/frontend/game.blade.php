@extends('frontend.layouts.base')

@section('content')
<h3>Playing Card Game</h3>
<h4>Player Name: {{$name}}</h4>
<hr>
<div class="row">
	<div id="game_board" class="col-md-7 no-float">
		@for ($i = 0; $i< Config::get('cg.deck.size.rows'); $i++)
			<div class="row">
				@for ($j = 0; $j< Config::get('cg.deck.size.cols'); $j++)
				<div class="col-xs-3">
					<a href="javascript:void(false)" class="thumbnail deck_card" data-row="{{$i}}" data-col="{{$j}}">
						<img src="{{asset('assets/img/flipped_card.png')}}" class="card_img">
					</a>
				</div>
				@endfor
			</div>
		@endfor
	</div>
	<div class="col-md-3 no-float col-md-offset-2">
		<div class='row'>
			<div class='col'>
				<div class='row'>
					<div class='col'>
						<h4>Time</h4>
					</div>
				</div>
				<div class='row'>
					<div id="time_counter" class='col'>
						<span></span>
					</div>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col'>
				<div class='row'>
					<div class='col'>
						<h4>Score</h4>
					</div>
					<div id="user_score" class='col'>
						<h4>0</h4>
					</div>
				</div>
				<div class='row'>
					<div class='col'>
						<h4>Total Score</h4>
					</div>
					<div id="user_total_score" class='col'>
						<h4>{{ $totalScore }}</h4>
					</div>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col'>
				<div class='row'>
					<div class='col'>
						<h4>Leader Board (Top 5)</h4>
					</div>
				</div>
				<div class='row'>
					<div class='col'>
						<table class="table table-hover">
							@foreach($topUsers as $userscore)
								<tr>
									<td>{{$userscore->user->name}}</td>
									<td>{{$userscore->score}}</td>
								</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$( document ).ready(function() {

		game.redirectEnd = '{{action('HomeController@end')}}';
		var interval = setInterval(function() {
			game.getTimer();
			if (game.startTime < 1) {
				window.location = game.redirectEnd;
				return;
			}
		}, 1000);

		$('div#game_board').length>0 && game.init();

		checkUrl = '{{url('game/flipcard')}}';

		$( "a.deck_card" ).on( "click", function( event ) {
			if ($(this).hasClass('visible')) {
				return false;
			}
			game.selectMove($(this), checkUrl);
			return false; // stop the browser following the link
		});
	});

</script>

@stop