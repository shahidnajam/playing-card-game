// the game...
var game = {
    // game items
    items: null,
    redirectEnd: null ,
    score: null, totalScore: null, gtime: null,

    // start time for elapsed time
    startTime: 120,
    // initialize
    init: function () {

        // set items
        this.items = $('div#game_board a');

        // set text elements
        this.totalScore = $('#user_total_score');
        this.score = $('#user_score');
        this.gtime = $('#time_counter span');
    },
    getTimer: function (){
        game.startTime = game.startTime - 1;
        if (game.startTime == -1) {
            //clearInterval(counter);
            return;
        }
        var seconds = game.startTime % 60;
        var minutes = Math.floor(game.startTime / 60);
        var hours = Math.floor(minutes / 60);
        minutes %= 60;
        hours %= 60;
        seconds = ("0" + seconds).slice(-2);

        timeText = minutes + ':' + seconds;
        // set timer text
        game.gtime.html('<h3>' + timeText + '</h3>');

    },
    // get visible items
    getVisible: function () {
        return this.items.filter('.visible');
    },
    selectMove: function (deckItem, checkUrl) {
        $.getJSON(checkUrl, {
            row: deckItem.data('row'),
            col: deckItem.data('col')
        }).done(function (data) {
            game.flipItem(deckItem, data.result.card_picture);
            game.score.html('<h3>' + data.result.score + '</h3>');

            visibleItems = game.getVisible();

            window.setTimeout(
                function () {
                    if (data.result.matched == 0) {
                        game.flipVisibleItem(visibleItems, data.result.flipped_picture);
                    }
                    else if (data.result.matched == 1) {
                        game.hideItem(visibleItems);
                        game.score.html('<h3>' + data.result.score + '</h3>');
                        game.totalScore.html('<h3>' + data.result.user_total_score + '</h3>');
                        if (data.result.game_over == 1) {
                            window.location = game.redirectEnd;
                        }
                    }

                },
                300
            );

        });
    },

    flipItem: function (item, newImgSrc) {
        imgObj = $('img', item);
        item.addClass('visible');
        imgObj.attr("src", newImgSrc);
        // this.changeImage(imgObj, newImgSrc);

    },
    flipVisibleItem: function (visibleItems, newImgSrc) {
        visibleItems.each(function () {
            imgObj = $('img', $(this));
            $(this).removeClass('visible');
            //imgObj.attr("src", 'assets/img/FLIPPED_OVER_CARD.png');
            game.changeImage(imgObj, newImgSrc);

        });
    },
    changeImage: function (imgObj, newSrc) {
        imgObj.fadeTo(200, 0.7, function () {
            imgObj.attr("src", newSrc);
        }).fadeTo("fast", 1);
    },
    // hide item
    hideItem: function (visibleItems) {
        imgObj = $('img', visibleItems);
        visibleItems.each(function () {
            $(this).attr('class', '');
            imgObj.fadeOut("slow", function() {
                $(this).remove();
            });
        });
    }
}
